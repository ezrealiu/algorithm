package com.simon.study.algorithm.tree;

import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * @author simon
 * @date 2022/8/22 4:23 上午
 */
public class TreeNodeDistance {

    /**  */
    public static NodeInfo maxDistanceNode(TNode root){
        if(root == null){
            return new NodeInfo(0, 0);
        }

        NodeInfo left = maxDistanceNode(root.left);
        NodeInfo right = maxDistanceNode(root.right);

        int ldist    = left.maxDistance ;
        int rdist    = right.maxDistance ;
        int dist     = left.height + 1 + right.height;


        NodeInfo distInfo = new NodeInfo();
        distInfo.maxDistance = Math.max(dist, Math.max(ldist, rdist));
        distInfo.height      = Math.max(left.height, right.height) + 1;

        return distInfo;
    }


    @NoArgsConstructor
    public static class NodeInfo {
        int height;
        int maxDistance;

        public NodeInfo(int height, int maxDistance) {
            this.height = height;
            this.maxDistance = maxDistance;
        }
    }
}

package com.simon.study.algorithm.tree;

import com.simon.study.algorithm.collect.ListCommon.LNode;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>
 *
 * @author simon
 * @date 2022/8/21 4:43 下午
 */
@NoArgsConstructor
public class Node implements INode {
    @Getter @Setter
    Integer data;

    public Node(Integer data) {
        this.data = data;
    }

    @Override
    public int compareTo(Node node) {
        return this.data.compareTo(node.data);
    }

    @Getter @Setter
    public static class LNode extends Node{
        LNode next;

        public LNode(Integer data) {
            super(data);
        }

        public String toString() {
            return "LN:" + (data == null ? "null" : data.toString());
        }
    }
}

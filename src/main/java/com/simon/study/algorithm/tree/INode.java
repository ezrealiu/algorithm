package com.simon.study.algorithm.tree;

/**
 * <p>
 *
 * @author simon
 * @date 2022/8/21 4:41 下午
 */
public interface INode extends Comparable<Node>{

    Integer getData();

}

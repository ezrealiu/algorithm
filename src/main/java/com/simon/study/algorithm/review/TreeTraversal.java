package com.simon.study.algorithm.review;

import com.simon.study.algorithm.tree.TNode;
import java.util.ArrayDeque;
import java.util.Deque;

/**
 * <p>
 *
 * @author SimonLiu
 * @date 2022/8/29 21:30
 */
public class TreeTraversal {

    public static void bfs(TNode root){
        if(root == null){ return; }

        Deque<TNode> stack = new ArrayDeque<>();
        stack.push(root);

        while (!stack.isEmpty()){
            TNode node = stack.pop();

            //todo sth.
            if(node.getLeft() != null){ stack.addLast(node.getLeft());  }
            if(node.getRight()!= null){ stack.addLast(node.getRight()); }
        }
    }


    public static void dfs(TNode root){
        if(root == null){ return; }

        Deque<TNode> stack = new ArrayDeque<>();
        stack.push(root);

        while (!stack.isEmpty()){
            TNode node = stack.pop();

            if(node.getRight() != null){ stack.push(node.getRight()); }
            if(node.getLeft()  != null){ stack.push(node.getLeft());  }
        }
    }
}

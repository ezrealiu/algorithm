package com.simon.study.algorithm.collect;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>
 *
 * @author simon
 * @date 2022/8/23 10:32 上午
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {
    private int capacity;

    public LRUCache(int capacity) {
        super(capacity, 0.75F, true);
        this.capacity = capacity;
    }

    protected boolean removeEldestEntry(Map.Entry eldest) {
        return size() > capacity;
    }
}

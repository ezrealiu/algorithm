package com.simon.study.algorithm.collect;

import java.util.Objects;

/**
 * <p>
 *
 * @author SimonLiu
 * @date 2022/8/30 22:22
 */
public class HashUtil {
    public static int mapHash(Object key){
        int h = 0;
        return (key == null) ? h : ((h = key.hashCode()) ^ (h>>>16));
    }

    public static int mapNodeHashcode(Object key, Object value){
        return Objects.hashCode(key) ^ Objects.hashCode(value);
    }

    public static int stringHashcode(String str){
        int h = 0;
        char[] chs = str.toCharArray();
        for (int i = 0; i < chs.length; i++) {
            h = 31 * h + chs[i];
        }
        return h;
    }
}

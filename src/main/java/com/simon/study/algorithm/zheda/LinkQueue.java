package com.simon.study.algorithm.zheda;

import com.simon.study.algorithm.tree.Node.LNode;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * @author simon
 */
@NoArgsConstructor
public class LinkQueue {
    LNode front;
    LNode rear;

    int     size;

    public boolean empty(){
        return front == null;
    }


    public boolean enque(int item){
        LNode node = new LNode(item);

        if(rear == null){
            front   = node;
        }else{
            rear.setNext(node);
        }
        rear    = node;
        this.size++;
        return true;
    }


    public LNode deque(){
        if(empty()){ return null; }
        LNode node  = front;
        front       = front.getNext();
        if(rear == node){ rear = front; }
        return node;
    }

}

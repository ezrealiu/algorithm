package com.simon.study.algorithm.zheda;

import lombok.Getter;

/**
 * <p>
 *
 * @author simon
 */
@Getter
public class ZAStack {

    int[]   elems;
    int     top;

    int     capacity;

    public ZAStack(int capacity) {
        this.capacity   = capacity;
        this.elems      = new int[capacity];
    }

    public boolean push(int item){
        if(full()){ return false; }

        elems[top++] = item;
        return true;
    }


    public Integer pop(){
        if(empty()){ return null; }

        int rst = elems[top--];
        return rst;
    }

    public boolean empty(){ return top == 0; }

    public boolean full(){
        return top == capacity;
    }
}

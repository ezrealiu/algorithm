package com.simon.study.algorithm.zheda;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * <p>
 *
 * @author simon
 */
@NoArgsConstructor
public class CyclicQueue {
    int[]   data;

    int     front;
    int     rear;

    @Getter
    int     capacity;


    public CyclicQueue(int capacity) {
        this.capacity   = capacity;
        this.data       = new int[capacity];
        this.front      = this.rear = -1;
    }


    public boolean full(){
        return ((rear + 1) % capacity) == front;
    }


    public boolean enque(int item){
        if(full()){ return false; }
        rear = (rear + 1) % capacity;
        data[rear] = item;
        return true;
    }

    public boolean empty(){
        return this.front == this.rear;
    }

    public int deque(){
        if(empty()){ return -1; }

        front = (front+1) % capacity;
        return data[front];
    }
}

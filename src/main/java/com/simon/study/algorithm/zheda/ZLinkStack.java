package com.simon.study.algorithm.zheda;

import com.simon.study.algorithm.tree.Node.LNode;

/**
 * <p>
 *
 * @author simon
 */
public class ZLinkStack {
    LNode   top;
    int     size;

    public ZLinkStack() { }

    public boolean push(int item){
        LNode node = new LNode(item);
        node.setNext(top);
        top = node;
        size++;
        return true;
    }


    public LNode pop(){
        LNode prev = top;
        top = top.getNext();
        size--;
        return prev;
    }
}

package com.simon.study.algorithm.zheda;

import lombok.Getter;

/**
 * <p>
 *
 * @author simon
 */
@Getter
public class ZArrayList {
    int[]   elems;
    int     size;
    int     capacity;


    public ZArrayList(int capacity) {
        this.capacity   = capacity;
        this.elems      = new int[capacity];
    }

    public int find(int item){
        int i = 0;
        while( i < size && elems[i] != item){
            i++;
        }
        if(size <= i){ return -1; }
        else return i;
    }


    public boolean insert(int i, int item){
        if( size == capacity ){ return false; }

        if(i > size || i < 0){ return false; }

        for (int j = size; j > i; j--) {
            elems[j] = elems[j-1];
        }
        elems[i] = item; size++;
        return true;
    }

    public boolean delete(int i){
        if(i >= size || i < 0){ return false; }

        size--;
        for (int j = i; j < size; j++) {
            elems[j] = elems[j+1];
        }

        return true;
    }

    public int findkth(int k){
        if(k < 0 || k >= size){ return -1; }
        return elems[k];
    }
}

package com.simon.study.algorithm.leetcode;

/**
 * <p>
 *
 * @author simon
 */
public class FindMedianInSortedArrays00004 {
    public static void main(String[] args) {
        int[] nums1 = new int[]{3, 8, 9, 12};
        int[] nums2 = new int[]{2, 4, 5, 10, 13, 14};

        System.out.println( median(nums1, nums2) );
    }


    public static double median(int[] nums1, int[] nums2){
        int size = nums1.length + nums2.length;
        int m    = nums1.length, n = nums2.length, p1 = 0, p2 = 0, k = size/2;
        int left = -1, right = -1;

        for (int i = 0; i <= k; i++) {
            left = right;
            if(p1 < m && ( (p2 >= n) || (nums1[p1] < nums2[p2])) ){
                right = nums1[p1++];
            }else{
                right = nums2[p2++];
            }
        }

        if( (size & 1) == 0 ){
            return (left + right)/2.0;
        }else{
            return right;
        }
    }
}

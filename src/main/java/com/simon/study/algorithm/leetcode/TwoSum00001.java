package com.simon.study.algorithm.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 *
 * @author simon
 */
public class TwoSum00001 {

    public static int[] solution(int[] nums, int target){
        Map<Integer,Integer> restIdxMap = new HashMap<>();
        int[] output = new int[2];

        for (int i = 0; i < nums.length; i++) {
            int rest  = target - nums[i];

            Integer otherIdx = restIdxMap.get( rest );

            if(Objects.isNull( otherIdx )){
                restIdxMap.put( nums[i], i );
            }else{
                output[0] = otherIdx;
                output[1] = i;

                System.out.println(Arrays.toString(output));
                break;
            }
        }
        return output;
    }

    public static void main(String[] args) {
        int[] n1 = new int[]{ 2, 7, 11, 15};
        solution(n1, 9);
    }
}

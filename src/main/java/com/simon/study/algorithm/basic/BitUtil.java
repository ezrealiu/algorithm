package com.simon.study.algorithm.basic;

/**
 * <p>
 *
 * @author simon
 * @date 2022/8/31 10:20 上午
 */
public class BitUtil {
    /** a == 2^n, b > 0 && b <= a*/
    public static int substract(int a, int b){
        return (0-b) & (a-1);
    }


    /** com == 2^n */
    public static int mod(int com, int key){
        return (com - 1) & key;
    }

    public static void main(String[] args) {
        System.out.println(15 & 87);
    }
}
